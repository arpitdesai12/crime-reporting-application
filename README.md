### What is this repository for? ###

* This is a Python based application to demonstarte feature of RPC using Python Spyne Library.

* This application shows crime activity in the area requested by user in format of latitude and longitude. 

* After running application use the following API to send requests 
  
  "http://localhost:8000/checkcrime?lat=37.334164&lon=-121.884301&radius=0.02" 
  
   Specify the lat and lon of the area you want to check crime and also radius within the area.

