import logging
logging.basicConfig(level=logging.DEBUG)
import re
from spyne import Application, rpc, ServiceBase, \
    Integer, Unicode

from streetaddress import StreetAddressFormatter, StreetAddressParser    
import dateutil.parser
from dateutil.tz import *
from datetime import *
import datetime
from spyne import Iterable
import requests 
import operator
from spyne.protocol.http import HttpRpc
from spyne.protocol.json import JsonDocument

from spyne.server.wsgi import WsgiApplication

class CheckCrimeService(ServiceBase):
    @rpc(str ,str,str, _returns=Iterable(str))
    def checkcrime(ctx, lat, lon, radius):
        total_crimes=0        
        crime = {}
        add={}
        
        three_one=dateutil.parser.parse("03:01", ignoretz=True)
        three_one=three_one.time()
        six=dateutil.parser.parse("06:00", ignoretz=True)
        six=six.time()
        six_one=dateutil.parser.parse("06:01", ignoretz=True)
        six_one=six_one.time()
        nine=dateutil.parser.parse("09:00", ignoretz=True)
        nine=nine.time()
        nine_one=dateutil.parser.parse("09:01", ignoretz=True)
        nine_one=nine_one.time()        
        twelve=dateutil.parser.parse("12:00", ignoretz=True)
        twelve=twelve.time()

        twelve_to_three_am=0
        three_to_six_am=0
        six_to_nine_am=0
        nine_to_twelve_noon=0

        twelve_to_three_pm=0
        three_to_six_pm=0
        six_to_nine_pm=0
        nine_to_twelve_midnight=0        

        parameters={"lat":lat, "lon":lon, "radius":radius,"key":"."}
        response = requests.get("https://api.spotcrime.com/crimes.json?",params=parameters)
        data = response.json()
        jsondata=data["crimes"]

        for item in jsondata:
            total_crimes+=1
            time=item.get("date")
            actualtime=time.split()
            timeval=actualtime[1]
            am_pm=actualtime[2]
            timeval=dateutil.parser.parse(timeval)
            timeval=timeval.time()
                   
            if am_pm =="AM":
                if timeval >= three_one and timeval <= six:
                    three_to_six_am = three_to_six_am + 1 
                elif timeval >= six_one and timeval <= nine:
                    six_to_nine_am =six_to_nine_am + 1
                elif timeval >= nine_one and timeval <twelve:
                    nine_to_twelve_noon =nine_to_twelve_noon + 1
                elif timeval==twelve:
                    nine_to_twelve_midnight =nine_to_twelve_midnight+ 1
                else :
                    twelve_to_three_am =twelve_to_three_am + 1
            else :
                if timeval >= three_one and timeval <= six:
                    three_to_six_pm = three_to_six_pm + 1 
                elif timeval >= six_one and timeval <= nine:
                    six_to_nine_pm =six_to_nine_pm + 1
                elif timeval >= nine_one and timeval < twelve:
                    nine_to_twelve_midnight =nine_to_twelve_midnight+ 1
                elif timeval==twelve:
                    nine_to_twelve_noon =nine_to_twelve_noon+ 1    
                else :
                    twelve_to_three_pm =twelve_to_three_pm + 1
                      
            type1=item.get("type")   
                                            
            if crime.has_key(type1):
                cval= crime.get(type1) +1
                crime.update({type1 : cval})
            else :
                crime.update({type1 : 1})
                     
            addressval=item.get("address")
            addr_parser = StreetAddressParser()
            addr=addr_parser.parse(addressval)
            add1=addr['street_full']
            patt=re.search("BLOCK",add1)
            patt1=re.search("&",add1)
            if patt :
                add1=re.sub("BLOCK ","",add1,2)
                add1=re.sub("OF ","",add1,2)
               
            if patt1 :               
                add2 = add1.split("&")                
                if add.has_key(add2[0]):
                    aval= add.get(add2[0]) +1
                    add.update({add2[0] : aval})
                else :
                    add.update({add2[0] : 1})

                if add.has_key(add2[1]):
                    aval= add.get(add2[1]) +1
                    add.update({add2[1] : aval})
                else :
                    add.update({add2[1] : 1})   
                                        
            elif add.has_key(add1):
                aval= add.get(add1) +1
                add.update({add1 : aval})
            else :
                add.update({add1 : 1})             
            
        sorted_x = sorted(add.items(), key=operator.itemgetter(1))  
        sorted_x.reverse()  
        the_most_dangerous_streets= [sorted_x[0][0],sorted_x[1][0],sorted_x[2][0]]

        event_time_count = {
        '12:01am-3am' : twelve_to_three_am,
        '3:01am-6am' : three_to_six_am,
        '6:01am-9am' : six_to_nine_am,
        '9:01am-12noon' : nine_to_twelve_noon,
        '12:01pm-3pm' : twelve_to_three_pm,
        '3:01pm-6pm' : three_to_six_pm,
        '6:01pm-9pm' : six_to_nine_pm,
        '9:01pm-12midnight' : nine_to_twelve_midnight
        } 

        output = {
        'total_crime' : total_crimes,        
        'event_time_count ' : event_time_count,
        'crime_type_count' : crime,
        'the_most_dangerous_streets' : the_most_dangerous_streets
        }
        yield output

application = Application([CheckCrimeService],
    tns='spyne.examples.hello',
    in_protocol=HttpRpc(validator='soft'),
    out_protocol=JsonDocument()
)

if __name__ == '__main__':
    # You can use any Wsgi server. Here, we chose
    # Python's built-in wsgi server but you're not
    # supposed to use it in production.
    from wsgiref.simple_server import make_server

    wsgi_app = WsgiApplication(application)
    server = make_server('0.0.0.0', 8000, wsgi_app)
    server.serve_forever()
